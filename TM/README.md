# CUCV TMs

This directory contains official technical manuals for the CUCV series trucks.

Title                                         | Manual                                               | Date
----------------------------------------------|------------------------------------------------------|-----------
Operator's Manual                             | [TM 9-2320-289-10](TM9-2320-289-10_1986-07-04.pdf)   | 1986-07-04
Unit Maintenance                              | [TM 9-2320-289-20](TM9-2320-289-20_1988-01-20.pdf)   | 1988-01-20
Unit Maintenance Parts                        | [TM 9-2320-289-20P](TM9-2320-289-20P_1992-05-01.pdf) | 1992-05-01
Direct and General Support Maintenance        | [TM 9-2320-289-34](TM9-2320-289-34_1989-01-12.pdf)   | 1989-01-12
Direct and General Support Maintenance Parts  | [TM 9-2320-289-34P](TM9-2320-289-34P_1992-05-01.pdf) | 1992-05-01
Direct and General Support Engine Maintenance | [TM 9-2815-237-34](TM9-2815-237-34_1996-01-31.pdf)   | 1996-01-31
Lubrication Order                             | [LO 9-2320-289-12](LO9-2320-289-12_1992-05-01.pdf)   | 1992-05-01
Transportability Guidance                     | [TM 55-2320-289-14](TM55-2320-289-14_1987-01-30.pdf) | 1987-01-30
