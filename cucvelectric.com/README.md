# cucvelectric.com Wiring Diagrams

This directory contains CUCV wiring diagrams downloaded from
cucvelectric.com in eary 2012. The site appears to be gone now.

Description                               | File
------------------------------------------|-------------------------------------------------------
Combined Wiring Diagrams (B&W PDF)        | [CUCV\_Wiring\_Diagrams.pdf](CUCV_Wiring_Diagrams.pdf)
Charging Circuits (Color JPEG)            | [cc.jpg](cc.jpg)
Charging Circuits (B&W PDF)               | [cc.pdf](cc.pdf)
Instrument Cluster Jack (Color JPEG)      | [dashplugone.jpg](dashplugone.jpg)
Instrument Cluster Jack (B&W PDF)         | [dp.pdf](dp.pdf)
Instrument Cluster Plug (Color JPEG)      | [dashplugtwo.jpg](dashplugtwo.jpg)
Instrument Cluster Plug (B&W PDF)         | [dpt.pdf](dpt.pdf)
Glow Plug Circuits (Color JPEG)           | [gpc.jpg](gpc.jpg)
Glow Plug Circuits (B&W PDF)              | [gpc.pdf](gpc.pdf)
Wire and Fuse Identification (Color JPEG) | [wireid.jpg](wireid.jpg)
Wire and Fuse Identification (Color PDF)  | [wireid.pdf](wireid.pdf)
