# Commercial Utility Cargo Vehicle (CUCV) Technical Manuals

This repository contains my curated cache of technical manuals for the
CUCV series of military vehicles. These vehicles were based on
Chevrolet pickup trucks and Blazer SUVs, with a whole bunch of extra
stuff added in order to provide 24VDC power to military radio
equipment, and to participate in slave starting with other tactical
vehicles with 24V electrical systems and NATO standard slave
starting receptacles.

Vehicles in the series include:

Name                                                   | Model   | NSN
-------------------------------------------------------|---------|-----------------
TRUCK, CARGO, TACTICAL, 1-1/4 TON, 4X4                 | M1008   | 2320-01-123-6827
TRUCK, CARGO, TACTICAL, 1-1/4 TON, 4X4                 | M1008A1 | 2320-01-123-2671
TRUCK, UTILITY, TACTICAL, 3/4 TON, 4X4                 | M1009   | 2320-01-123-2665
TRUCK, AMBULANCE, TACTICAL, 1-1/4 TON, 4X4             | M1010   | 2310-01-123-2666
TRUCK, SHELTER CARRIER, TACTICAL, 1-1/4 TON, 4X4       | M1028   | 2320-01-127-5077
TRUCK, SHELTER CARRIER W/PTO, TACTICAL, 1-1/4 TON, 4X4 | M1028A1 | 2320-01-158-0820
TRUCK, SHELTER CARRIER W/PTO, TACTICAL 1-1/4 TON, 4X4  | M1028A2 | 2320-01-295-0822
TRUCK, SHELTER CARRIER, TACTICAL, 1-1/4 TON, 4X4       | M1028A3 | 2320-01-325-1937
TRUCK, CHASSIS, TACTICAL, 1-1/4 TON, 4X4               | M1031   | 2320-01-133-5368


The documents are sorted into these subdirectories:


Directory                                      | Description
-----------------------------------------------|----------------------------------------------------------
[TM](TM/README.md)                             | Official US military Technical Manuals
[cucvelectric.com](cucvelectric.com/README.md) | Wiring diagrams downloaded from cucvelectric.com in 2012
[misc](misc/README.md)                         | Miscellaneous documents

